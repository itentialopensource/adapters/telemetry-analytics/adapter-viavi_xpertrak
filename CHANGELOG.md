
## 0.4.4 [10-14-2024]

* Changes made at 2024.10.14_18:38PM

See merge request itentialopensource/adapters/adapter-viavi_xpertrak!13

---

## 0.4.3 [09-19-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-viavi_xpertrak!11

---

## 0.4.2 [08-14-2024]

* Changes made at 2024.08.14_17:18PM

See merge request itentialopensource/adapters/adapter-viavi_xpertrak!10

---

## 0.4.1 [08-07-2024]

* Changes made at 2024.08.06_18:18PM

See merge request itentialopensource/adapters/adapter-viavi_xpertrak!9

---

## 0.4.0 [08-05-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/telemetry-analytics/adapter-viavi_xpertrak!8

---

## 0.3.5 [03-27-2024]

* Changes made at 2024.03.27_13:05PM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-viavi_xpertrak!7

---

## 0.3.4 [03-12-2024]

* Changes made at 2024.03.12_10:54AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-viavi_xpertrak!6

---

## 0.3.3 [02-27-2024]

* Changes made at 2024.02.27_11:26AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-viavi_xpertrak!5

---

## 0.3.2 [01-23-2024]

* Fix body

See merge request itentialopensource/adapters/telemetry-analytics/adapter-viavi_xpertrak!4

---

## 0.3.1 [01-09-2024]

* fix links

See merge request itentialopensource/adapters/telemetry-analytics/adapter-viavi_xpertrak!3

---

## 0.3.0 [01-05-2024]

* more migration changes

See merge request itentialopensource/adapters/telemetry-analytics/adapter-viavi_xpertrak!2

---

## 0.2.0 [01-05-2024]

* 2023 Adapter Migration

See merge request itentialopensource/adapters/telemetry-analytics/adapter-viavi_xpertrak!1

---

## 0.1.1 [11-16-2023]

* Bug fixes and performance improvements

See commit e02bf13

---

## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for VIAVI XPERTrak. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for VIAVI XPERTrak.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Viavi_xpertrak. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getAllCmtsUsingGET1(callback)</td>
    <td style="padding:15px">getAllCmtsUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/cmts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addPublicCmtsUsingPOST1(body, callback)</td>
    <td style="padding:15px">addPublicCmtsUsingPOST_1</td>
    <td style="padding:15px">{base_path}/{version}/cmts/add?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllCmtsLiteUsingGET1(callback)</td>
    <td style="padding:15px">getAllCmtsLiteUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/cmts/allLite?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">processCmtsCsvUsingPOST1(file, callback)</td>
    <td style="padding:15px">processCmtsCsvUsingPOST_1</td>
    <td style="padding:15px">{base_path}/{version}/cmts/import/csv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">syncCmtsUsingPOST1(body, callback)</td>
    <td style="padding:15px">syncCmtsUsingPOST_1</td>
    <td style="padding:15px">{base_path}/{version}/cmts/sync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCmtsUsingGET1(identifier, callback)</td>
    <td style="padding:15px">getCmtsUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/cmts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editCmtsUsPortNameUsingPOST1(body, callback)</td>
    <td style="padding:15px">editCmtsUsPortNameUsingPOST_1</td>
    <td style="padding:15px">{base_path}/{version}/cmtsusport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCmtsUSPortsCSVUsingGET1(callback)</td>
    <td style="padding:15px">getCmtsUSPortsCSVUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/cmtsusport/csv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editCmtsUsPortNameCSVUsingPOST1(csvFile, callback)</td>
    <td style="padding:15px">editCmtsUsPortNameCSVUsingPOST_1</td>
    <td style="padding:15px">{base_path}/{version}/cmtsusport/csv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllHcusUsingGET1(callback)</td>
    <td style="padding:15px">getAllHcusUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/hcu?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">processHcuCsvUsingPOST2(file, callback)</td>
    <td style="padding:15px">processHcuCsvUsingPOST_2</td>
    <td style="padding:15px">{base_path}/{version}/hcu/import/csv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHcuUsingGET1(identifier, callback)</td>
    <td style="padding:15px">getHcuUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/hcu/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllHsmsUsingGET1(callback)</td>
    <td style="padding:15px">getAllHsmsUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/hsm?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHsmUsingGET1(identifier, callback)</td>
    <td style="padding:15px">getHsmUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/hsm/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllRpmsUsingGET1(limit, callback)</td>
    <td style="padding:15px">getAllRpmsUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/rpm?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRpmUsingGET1(identifier, callback)</td>
    <td style="padding:15px">getRpmUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/rpm/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllRpmPortsUsingGET1(limit, callback)</td>
    <td style="padding:15px">getAllRpmPortsUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/rpmport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRpmPortUsingGET1(identifier, callback)</td>
    <td style="padding:15px">getRpmPortUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/rpmport/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodesUsingGET2(callback)</td>
    <td style="padding:15px">getNodesUsingGET_2</td>
    <td style="padding:15px">{base_path}/{version}/node?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">editNodeNameUsingPOST2(body, callback)</td>
    <td style="padding:15px">editNodeNameUsingPOST_2</td>
    <td style="padding:15px">{base_path}/{version}/node/name?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeQoeHistoryUsingGET1(duration, identifier, sampleResponse, startdatetime, callback)</td>
    <td style="padding:15px">getNodeQoeHistoryUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/node/{pathv1}/qoe/metric/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeSummaryMetricUsingGET1(identifier, sampleResponse, startdatetime, callback)</td>
    <td style="padding:15px">getNodeSummaryMetricUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/node/{pathv1}/summary/metric?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getByStatusUsingGET1(status, callback)</td>
    <td style="padding:15px">getByStatusUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/noiseTrak/legGroupInfo/status/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllOtusUsingGET1(callback)</td>
    <td style="padding:15px">getAllOtusUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/otu?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOtuUsingGET1(otuId, callback)</td>
    <td style="padding:15px">getOtuUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/otu/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllPortsUsingGET1(callback)</td>
    <td style="padding:15px">getAllPortsUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/otuPort?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllRcisUsingGET1(callback)</td>
    <td style="padding:15px">getAllRcisUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/rci?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRciUsingGET1(identifier, callback)</td>
    <td style="padding:15px">getRciUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/rci/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loginUsingPOST1(feature, body, callback)</td>
    <td style="padding:15px">loginUsingPOST_1</td>
    <td style="padding:15px">{base_path}/{version}/auth/login?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">logoutUsingGET1(callback)</td>
    <td style="padding:15px">logoutUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/auth/logout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getModemInclusionsExclusionsUsingGET1(callback)</td>
    <td style="padding:15px">getModemInclusionsExclusionsUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/modem/exclusion?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getModemExclusionDescriptionsUsingGET1(callback)</td>
    <td style="padding:15px">getModemExclusionDescriptionsUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/modem/exclusion/descriptions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getModemKpiPerformanceHistoryUsingGET3(fREQ, kPI, datetime, duration, identifiers, callback)</td>
    <td style="padding:15px">getModemKpiPerformanceHistoryUsingGET_3</td>
    <td style="padding:15px">{base_path}/{version}/modem/kpi/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getModemKpiTypesUsingGET1(callback)</td>
    <td style="padding:15px">getModemKpiTypesUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/modem/kpi/types?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getModemDescriptionUsingGET1(callback)</td>
    <td style="padding:15px">getModemDescriptionUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/modem/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getModemWatchUsingGET1(callback)</td>
    <td style="padding:15px">getModemWatchUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/modemwatch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getModemWatchByNodeUsingGET1(identifier, callback)</td>
    <td style="padding:15px">getModemWatchByNodeUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/modemwatch/node/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteModemWatchByNodeUsingDELETE1(identifier, callback)</td>
    <td style="padding:15px">deleteModemWatchByNodeUsingDELETE_1</td>
    <td style="padding:15px">{base_path}/{version}/modemwatch/node/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllCollectionsUsingGET1(callback)</td>
    <td style="padding:15px">getAllCollectionsUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/schedule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScheduleByHourForCollectionUsingGET1(collectionType, callback)</td>
    <td style="padding:15px">getScheduleByHourForCollectionUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/schedule/hour/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScheduleForCollectionUsingGET1(collectionType, callback)</td>
    <td style="padding:15px">getScheduleForCollectionUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/schedule/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodeContainersUsingGET1(callback)</td>
    <td style="padding:15px">getNodeContainersUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/container/nodeContainers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bulkUpdateDeviceAttributesUsingPOST1(body, callback)</td>
    <td style="padding:15px">bulkUpdateDeviceAttributesUsingPOST_1</td>
    <td style="padding:15px">{base_path}/{version}/deviceSettings/bulkUpdate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bulkDeleteDeviceAttributesUsingDELETE1(callback)</td>
    <td style="padding:15px">bulkDeleteDeviceAttributesUsingDELETE_1</td>
    <td style="padding:15px">{base_path}/{version}/deviceSettings/reset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllAlarmsUsingGET1(element, limit, mostRecentAlarmIsoDate, totalAlarmsLastReceived, type, callback)</td>
    <td style="padding:15px">getAllAlarmsUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/alarm/all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">clearAlarmsUsingDELETE1(callback)</td>
    <td style="padding:15px">clearAlarmsUsingDELETE_1</td>
    <td style="padding:15px">{base_path}/{version}/alarm/clear?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">clearAllElementAlarmsUsingDELETE1(element, type, callback)</td>
    <td style="padding:15px">clearAllElementAlarmsUsingDELETE_1</td>
    <td style="padding:15px">{base_path}/{version}/alarm/clear/all/element?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">clearAllNetworkAlarmsUsingDELETE1(element, type, callback)</td>
    <td style="padding:15px">clearAllNetworkAlarmsUsingDELETE_1</td>
    <td style="padding:15px">{base_path}/{version}/alarm/clear/all/network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getElementAlarmsUsingGET1(element, limit, mostRecentAlarmIsoDate, totalAlarmsLastReceived, type, callback)</td>
    <td style="padding:15px">getElementAlarmsUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/alarm/element?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworkAlarmsUsingGET1(element, limit, mostRecentAlarmIsoDate, totalAlarmsLastReceived, type, callback)</td>
    <td style="padding:15px">getNetworkAlarmsUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/alarm/network?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEventsUsingGET2(id, type, callback)</td>
    <td style="padding:15px">getEventsUsingGET_2</td>
    <td style="padding:15px">{base_path}/{version}/event/element/type/{pathv1}/id/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryUsingPOST1(body, callback)</td>
    <td style="padding:15px">queryUsingPOST_1</td>
    <td style="padding:15px">{base_path}/{version}/graphql?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMactrakDetailsUsingGET1(duration, limit, node, nodeId, nodeLimit, startTime, callback)</td>
    <td style="padding:15px">getMactrakDetailsUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/mactrakdetail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getChannelPerformanceUsingGET2(duration, limit, portId, startDateTime, callback)</td>
    <td style="padding:15px">getChannelPerformanceUsingGET_2</td>
    <td style="padding:15px">{base_path}/{version}/mactrakperformance/cp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodePerformanceUsingGET2(duration, limit, portid, startDateTime, callback)</td>
    <td style="padding:15px">getNodePerformanceUsingGET_2</td>
    <td style="padding:15px">{base_path}/{version}/mactrakperformance/np/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPacketsUsingGET2(duration, limit, portId, startDateTime, callback)</td>
    <td style="padding:15px">getPacketsUsingGET_2</td>
    <td style="padding:15px">{base_path}/{version}/mactrakperformance/packets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLatestRankingsUsingGET2(duration, failedOnly, limit, node, nodeId, nodeLimit, startTime, callback)</td>
    <td style="padding:15px">getLatestRankingsUsingGET_2</td>
    <td style="padding:15px">{base_path}/{version}/mactrakranking?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">startLiveViewUsingPUT1(userId, body, callback)</td>
    <td style="padding:15px">startLiveViewUsingPUT_1</td>
    <td style="padding:15px">{base_path}/{version}/kpi/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTraceUsingGET5(sequencenumber, sessionid, callback)</td>
    <td style="padding:15px">getTraceUsingGET_5</td>
    <td style="padding:15px">{base_path}/{version}/kpi/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getModemKpiPerformanceHistoryUsingGET2(datetime, duration, nodeId, callback)</td>
    <td style="padding:15px">getModemKpiPerformanceHistoryUsingGET_2</td>
    <td style="padding:15px">{base_path}/{version}/kpi/history/modemResets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importNoiseTrakJsonUsingPOST2(isToBePersisted, sampleResponse, body, callback)</td>
    <td style="padding:15px">importNoiseTrakJsonUsingPOST_2</td>
    <td style="padding:15px">{base_path}/{version}/noiseTrakImport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">processCsvImportUsingPOST2(csvFile, importConfig, isToBePersisted, sampleResponse, callback)</td>
    <td style="padding:15px">processCsvImportUsingPOST_2</td>
    <td style="padding:15px">{base_path}/{version}/noiseTrakImport/csvCustomParser?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">csvParserConfigUsingGET2(callback)</td>
    <td style="padding:15px">csvParserConfigUsingGET_2</td>
    <td style="padding:15px">{base_path}/{version}/noiseTrakImport/csvParserConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSpectrumDetailsUsingGET3(duration, limit, node, nodeId, nodeLimit, startTime, callback)</td>
    <td style="padding:15px">getSpectrumDetailsUsingGET_3</td>
    <td style="padding:15px">{base_path}/{version}/qoesummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getQoeNodeRankingsUsingGET1(duration, failedOnly, limit, node, nodeId, nodeLimit, startTime, callback)</td>
    <td style="padding:15px">getQoeNodeRankingsUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/qoeranking?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disableSweepUsingPUT3(cmtsusportid, callback)</td>
    <td style="padding:15px">disableSweepUsingPUT_3</td>
    <td style="padding:15px">{base_path}/{version}/rcisweep/disablesweep/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableSweepUsingPOST3(body, callback)</td>
    <td style="padding:15px">enableSweepUsingPOST_3</td>
    <td style="padding:15px">{base_path}/{version}/rcisweep/enablesweep?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rCISweepPlanAPI1(callback)</td>
    <td style="padding:15px">RCI Sweep Plan API_1</td>
    <td style="padding:15px">{base_path}/{version}/rcisweep/getsweepplans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getElementBroadcastStatusResponsesUsingGET1(id, sampleResponse, type, callback)</td>
    <td style="padding:15px">getElementBroadcastStatusResponsesUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/broadcast?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putRpmPortBroadcastUsingPUT2(identifier, sampleResponse, body, callback)</td>
    <td style="padding:15px">putRpmPortBroadcastUsingPUT_2</td>
    <td style="padding:15px">{base_path}/{version}/broadcast/type/port/id/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRpmPortBroadcastUsingDELETE1(identifier, callback)</td>
    <td style="padding:15px">deleteRpmPortBroadcastUsingDELETE_1</td>
    <td style="padding:15px">{base_path}/{version}/broadcast/type/port/id/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRankingDetailsUsingGET2(area, limit, rankingCategory, sampleResponse, callback)</td>
    <td style="padding:15px">getRankingDetailsUsingGET_2</td>
    <td style="padding:15px">{base_path}/{version}/ranking/details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRankingSummaryUsingGET1(rankingCategory, sampleResponse, callback)</td>
    <td style="padding:15px">getRankingSummaryUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/summary/ranking?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchAlarmableModemsUsingGET1(callback)</td>
    <td style="padding:15px">searchAlarmableModemsUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/elements/search/type/alarmablemodem/**?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchModemWatchModemsUsingGET1(callback)</td>
    <td style="padding:15px">searchModemWatchModemsUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/elements/search/type/modemwatch/**?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchElementsUsingGET1(type, callback)</td>
    <td style="padding:15px">searchElementsUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/elements/search/type/{pathv1}/**?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getElementDetailUsingGET1(allRelated, id, type, callback)</td>
    <td style="padding:15px">getElementDetailUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/elements/type/{pathv1}/id/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllUsingGET6(callback)</td>
    <td style="padding:15px">getAllUsingGET_6</td>
    <td style="padding:15px">{base_path}/{version}/site?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSpectrumDetailsUsingGET4(duration, levelTypes, limit, node, nodeId, nodeLimit, startTime, callback)</td>
    <td style="padding:15px">getSpectrumDetailsUsingGET_4</td>
    <td style="padding:15px">{base_path}/{version}/spectrumall?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSpectrumDetailsUsingGET5(duration, limit, node, nodeId, nodeLimit, showLevels, startTime, callback)</td>
    <td style="padding:15px">getSpectrumDetailsUsingGET_5</td>
    <td style="padding:15px">{base_path}/{version}/spectrumdetail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLatestRankingsUsingGET3(duration, failedOnly, limit, node, nodeId, nodeLimit, startTime, threshold, callback)</td>
    <td style="padding:15px">getLatestRankingsUsingGET_3</td>
    <td style="padding:15px">{base_path}/{version}/spectrumranking?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">importTopologyJsonUsingPOST1(body, callback)</td>
    <td style="padding:15px">importTopologyJsonUsingPOST_1</td>
    <td style="padding:15px">{base_path}/{version}/topology/import?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">processCsvUsingPOST3(action, address, addressLine2, city, country, cpe, customField1, customField2, customField3, customField4, customField5, customField6, customerId, customerName, elementType, file, header, isPersisted, latitude, longitude, make, model, name, nodeName, parentName, phoneNumber, powerSupply, separator, serviceTier, state, zip, callback)</td>
    <td style="padding:15px">processCsvUsingPOST_3</td>
    <td style="padding:15px">{base_path}/{version}/topology/import/csv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">processCsvUsingPOST2(csvFile, importConfig, isPersisted, callback)</td>
    <td style="padding:15px">processCsvUsingPOST_2</td>
    <td style="padding:15px">{base_path}/{version}/topology/import/csvAndConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">csvParserConfigUsingGET3(callback)</td>
    <td style="padding:15px">csvParserConfigUsingGET_3</td>
    <td style="padding:15px">{base_path}/{version}/topology/import/topologyConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllFeatureNamesUsingGET1(callback)</td>
    <td style="padding:15px">getAllFeatureNamesUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/userActionLog/featureNames?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsingGET2(datetime, duration, feature, limit, targetId, userId, callback)</td>
    <td style="padding:15px">getUsingGET_2</td>
    <td style="padding:15px">{base_path}/{version}/userActionLog/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsingGET3(datetime, duration, feature, callback)</td>
    <td style="padding:15px">getUsingGET_3</td>
    <td style="padding:15px">{base_path}/{version}/useractionsummary/filter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRankingUsingGET1(limit, callback)</td>
    <td style="padding:15px">getRankingUsingGET_1</td>
    <td style="padding:15px">{base_path}/{version}/preeq/node?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
